#pragma once

#include "ListaEncadeada.h"

// typedef TabelaConteudo ** ListaEncadeada; // Lista de ponteiros de TabelaConteudo

class TabelaDispersao
{
private:
	ListaEncadeada ** diretorio;	// Vetor de ponteiros de ListaEncadeada
	ListaEncadeada ** diretorioExtra;
	
	float alpha;
	int contadorX;
	int contagemElementos;
	int maximoElementos;
	int tamanhoBase;
	int contadorDiretorio;
	bool expansao;

	ListaEncadeada * tabelaAtual;

	int H();
	void UpdateAlpha();
	void MAllocDiretorio();
	void SetTabelaAtual(ListaEncadeada * atual);

public:
	TabelaDispersao();
	~TabelaDispersao();

	void push(int valor);
	void pop(int valor);


};