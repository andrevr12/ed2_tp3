#include "TabelaDispersao.h"

int TabelaDispersao::H()
{
	return 0;
}

void TabelaDispersao::UpdateAlpha()
{
	alpha = (contagemElementos * 1.0f) / maximoElementos;

	if ((int)alpha == 1)
	{
		expansao = true;
	}
}

void TabelaDispersao::MAllocDiretorio()
{
	contadorDiretorio++;
	diretorio[contadorDiretorio] = new ListaEncadeada[maximoElementos];
}

void TabelaDispersao::SetTabelaAtual(ListaEncadeada * atual)
{
	tabelaAtual = atual;
}

TabelaDispersao::TabelaDispersao()
{
	expansao = false;
	tamanhoBase = 23;
	maximoElementos = tamanhoBase;
	contagemElementos = 0;
	contadorDiretorio = -1;
	diretorio = new ListaEncadeada*[3];

	UpdateAlpha();
	MAllocDiretorio();
}

TabelaDispersao::~TabelaDispersao()
{
}

void TabelaDispersao::push(int valor)
{
	contagemElementos++;
	UpdateAlpha();

	int tabPos = valor % maximoElementos;
	int dirPos = tabPos / (maximoElementos-1);

	ListaConteudo * obj = new ListaConteudo;
	obj->anterior = nullptr;
	obj->proximo = nullptr;
	obj->chave = valor;

	diretorio[dirPos][tabPos].push_back(obj);
}

void TabelaDispersao::pop(int valor)
{
}
