#pragma once

struct ListaConteudo
{
	ListaConteudo * anterior;
	int chave;
	ListaConteudo * proximo;
};

enum ErrorList
{
	SUCCESS,
	UNKNOWN_ERROR,
	NODE_NOT_FOUND,
	VOID_LIST
};

class ListaEncadeada
{
private:
	ListaConteudo * frontConteudo;
	ListaConteudo * backConteudo;

	int contagemElementos;

public:
	ListaEncadeada();
	~ListaEncadeada();

	void push_front(ListaConteudo * item);
	void push_back(ListaConteudo * item);
	ErrorList pop_front();
	ErrorList pop_back();
	ErrorList remove(ListaConteudo * item);

};