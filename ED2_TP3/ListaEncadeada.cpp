#include "ListaEncadeada.h"

ListaEncadeada::ListaEncadeada()
{
	contagemElementos = 0;
}

ListaEncadeada::~ListaEncadeada()
{
}

void ListaEncadeada::push_front(ListaConteudo * item)
{
	if (frontConteudo)
	{
		item->proximo = frontConteudo;
		frontConteudo->anterior = item;
	}
	else
		frontConteudo = item;
}

void ListaEncadeada::push_back(ListaConteudo * item)
{
	if (frontConteudo)
	{
		ListaConteudo * iterator = frontConteudo;

		for (size_t i = 0; i < contagemElementos; i++)
			iterator = iterator->proximo;

		iterator->proximo = item;
		item->anterior = iterator;
	}
	else
		frontConteudo = item;
}

ErrorList ListaEncadeada::pop_front()
{
	ErrorList err;

	if (frontConteudo)
	{
		ListaConteudo * postConteudo = frontConteudo->proximo;
		delete frontConteudo;
		frontConteudo = postConteudo;
		err = SUCCESS;
	}
	else
		err = VOID_LIST;

	return err;
}

ErrorList ListaEncadeada::pop_back()
{
	ErrorList err;

	if (frontConteudo)
	{
		ListaConteudo * iterator = frontConteudo;

		for (size_t i = 0; i < contagemElementos; i++)
			iterator = iterator->proximo;

		iterator->anterior->proximo = nullptr;
		delete iterator;
		
		err = SUCCESS;
	}
	else
		err = VOID_LIST;

	return err;
}

ErrorList ListaEncadeada::remove(ListaConteudo * item)
{
	ErrorList err;

	if (frontConteudo)
	{
		if (frontConteudo == item)
		{
			// pop_front();
			ListaConteudo * postConteudo = frontConteudo->proximo;
			delete frontConteudo;
			frontConteudo = postConteudo;
			err = SUCCESS;
		}
		else
		{
			ListaConteudo * iterator = frontConteudo->proximo;
			bool loop = true;

			for (size_t i = 1; loop && i < contagemElementos; i++)
			{
				if (iterator == item || iterator == nullptr)
					loop = false;
				else
				{
					iterator = iterator->proximo;
				}
			}

			if (iterator)
			{
				ListaConteudo * itFront = iterator->anterior;
				ListaConteudo * itBack = iterator->proximo;

				itFront->proximo = itBack;
				itBack->anterior = itFront;

				delete iterator;

				err = SUCCESS;
			}
			else
				err = NODE_NOT_FOUND;
		}		
	}
	else
		err = VOID_LIST;

	return err;
}
